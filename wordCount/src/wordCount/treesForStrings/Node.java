/*
class that stores the B-Number
data type of the B-Number should be an int
Assume a student record just has a B-Number
*/

package wordCount.treesForStrings;
import java.util.ArrayList;

public class Node {
	String word;
	Node left;
	Node right;
	boolean seen;
	int frequency;
	public Node(String wordIn) {
		word = wordIn;
		this.left = null;
		this.right = null;
		frequency = 1;
		this.seen = false;
	}


}
