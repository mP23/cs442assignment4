package wordCount.treesForStrings;
import java.util.*;

import wordCount.visitors.TreeVisitorI;

public class BST{

	private Node root;
	private Node realMax;
	int max;
	ArrayList<String> al = new ArrayList<String>();

	public BST(){
		root = null;
		max = 0;
	}

	public void insert(String wordIn){
		String wordProcessed = wordIn.toLowerCase();
		if(root == null){
			Node nn = new Node(wordProcessed);
			root = nn;
		}
		else{
			insert(root, wordProcessed);
		}
	}

	private void insert(Node nd, String wordIn){
		int result;
		Node temp;
		if(search(wordIn) == null){
			if((result = wordIn.compareTo(nd.word)) > 0){
				if(nd.right == null){
					temp = new Node(wordIn);
					nd.right = temp;
					return;
				}
				else{
					insert(nd.right, wordIn);
					return;
				}
			}
			else{
				if(nd.left == null){
					temp = new Node(wordIn);
					nd.left = temp;
					return;
				}
				else{
					insert(nd.left, wordIn);
					return;
				}
			}
		}
		else{
			search(wordIn).frequency+=1;
			if((search(wordIn).frequency) > max) {
				max = search(wordIn).frequency;
				//System.out.println(search(wordIn).word);
				realMax = search(wordIn);
				//System.out.println(realMax.word);
			}
		}
	}

	public Node search(String wordIn){
		String wordProcessed;
		if(root == null){
			return null;
		}
		else{
			wordProcessed = wordIn.toLowerCase();
			return search(root, wordProcessed);
		}
	}

	private Node search(Node nd, String wordIn){
		int result;
		if(nd == null){
			return null;
		}
		if((result = wordIn.compareTo(nd.word)) == 0){
			//System.out.println(nd.word);
			return nd;
		}
		if((result = wordIn.compareTo(nd.word)) > 0){
			return search(nd.right, wordIn);
		}
		if((result = wordIn.compareTo(nd.word)) < 0){
			return search(nd.left, wordIn);
		}
		return null;

	}
	
	public void printInOrder(){
		if(root == null){
			return;
		}
		else{
			printInOrder(root);
		}
	}

	private void printInOrder(Node nd){
		if(nd == null){
			return;
		}
		printInOrder(nd.left);
		al.add(nd.word);
		System.out.printf("%s, %d\n", nd.word, nd.frequency);
		printInOrder(nd.right);
	}

	public int getWordCount(){
		if(root == null){
			return 0;
		}
		else{
			return getWordCount(root);
		}
	}

	private int getWordCount(Node nd){
		if(nd == null){
			return 0;
		}
		int left = getWordCount(nd.left);
		int right = getWordCount(nd.right);

		return nd.frequency + right + left;
	}
	
	public String getWordFrequency() {
		if(root == null) {
			return null;
		}
		else{
			return getWordFrequency(root);
		}
	}
	
	private String getWordFrequency(Node nd) {
		//each node has a frequency
		if(nd == null) {
			return null;
		}
		//System.out.println(realMax);
		return realMax.word;
	}
	
	public int getFrequencyCount() {
		if (root == null) {
			return 0;
		}
		else {
			return getFrequencyCount(root);
		}
	}
	
	private int getFrequencyCount(Node nd) {
		if (nd == null) {
			return 0;
		}
		//System.out.println(max);
		return max;			
	}

	public int getCharacterCount() {		
		String s = "";
		for(String t: al) {
			s += t;
		}
		int count = s.length();
		return count;		
	}
	
	public int grep(String wordIn) {
		if (root == null) {
			return 0;
		}
		else {
			return grep(wordIn, root);
		}
	}
	
	private int grep(String wordIn, Node nd) {
		int someCounter = 0;
		if(nd == null) {
			return 0;
		}
		if(search(wordIn) != null) {
			if(search(wordIn).frequency > someCounter) {
				someCounter = search(wordIn).frequency;
			}
		}
		if(search(wordIn) == null) {
			return 0;
		}
		return someCounter;
	}
	
	public void accept(TreeVisitorI visitor) {
		visitor.visit(this);
	}
}
