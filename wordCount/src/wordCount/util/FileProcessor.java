package wordCount.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.*;
import java.io.File;
import java.nio.file.*;
import java.io.IOException;

public class FileProcessor{
	
	BufferedReader in = null;
	public FileProcessor(String fileName) {
		try {
			Path path = Paths.get(fileName);
			in = Files.newBufferedReader(path);
		}
		catch (Exception e) {
			System.err.println("The error is: " + e);
			e.printStackTrace();
		}
	}

	public String readLineFromFile(){
		String line;
		try {
			line = in.readLine();
		}
		catch (IOException e) {
			return null;
		}
		return line;
	}

}
