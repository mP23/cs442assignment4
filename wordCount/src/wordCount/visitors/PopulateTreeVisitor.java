package wordCount.visitors;

import wordCount.util.FileProcessor;
import wordCount.treesForStrings.BST;

public class PopulateTreeVisitor implements TreeVisitorI {
	FileProcessor fp;
	BST bst;
	public PopulateTreeVisitor(FileProcessor fpIn){
		fp = fpIn;

	}

	public void visit(BST bstIn) {
		bst = bstIn;
		String line;
		String[] words; 
		while((line = fp.readLineFromFile()) != null){
			words = line.split("\\s+");	
			for(int i = 0; i < words.length; ++i){
				//System.out.println(words[i]);
				if(!words[i].isEmpty()) {
					bst.insert(words[i]);
				}
			}
		}
		//System.out.println("-------");
		bst.printInOrder();
		return;
	}
	
}
