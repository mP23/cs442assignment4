
package wordCount.visitors;
import wordCount.treesForStrings.BST;
import java.util.*;
import java.io.*;

public class WordCountVisitor implements TreeVisitorI {
	PrintWriter out = null;
	FileWriter fw = null;
	BufferedWriter bw = null;
	String outputFile;
	public WordCountVisitor(String outputIn) {
		outputFile = outputIn;
		try{
			fw = new FileWriter(outputFile, true);
			bw = new BufferedWriter(fw);
			out = new PrintWriter(bw);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void visit(BST bst) {
		out.print("The total number of words is: ");
		out.print(bst.getWordCount());
		out.print("\n");
		out.print("The most frequently used word is: ");
		out.print(bst.getWordFrequency());
		out.print("\n");
		out.print("The frequency of the most frequently used word is: ");
		out.print(bst.getFrequencyCount());
		out.print("\n");
		out.print("The number of characters (without whitespaces) is: ");
		out.print(bst.getCharacterCount());
		out.print("\n");
		out.close();
	}
}
		
