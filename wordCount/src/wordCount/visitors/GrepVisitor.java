package wordCount.visitors;

import wordCount.treesForStrings.BST;
import java.util.*;
import java.io.*;
import wordCount.visitors.WordCountVisitor;

public class GrepVisitor implements TreeVisitorI {
	PrintWriter out = null;
	BufferedWriter bw = null;
	FileWriter fw = null;
	String word;
	String outputFile;
	
	public GrepVisitor(String outputIn, String wordIn) {
		word = wordIn;
		outputFile = outputIn;
	}
	
	public void visit(BST bst) {
		try{
			fw = new FileWriter(outputFile, true);
			bw = new BufferedWriter(fw);
			out = new PrintWriter(bw);
			out.print("The word " + word + " occurs the following times: ");
			out.print(bst.grep(word));
			out.print("\n");
			out.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}

