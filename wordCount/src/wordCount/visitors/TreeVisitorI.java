
package wordCount.visitors;
import wordCount.treesForStrings.BST;

public interface TreeVisitorI {
	public void visit(BST bst);
}

