 package wordCount.driver;

import wordCount.util.FileProcessor;
import wordCount.treesForStrings.BST;
import wordCount.treesForStrings.Node;
import wordCount.visitors.WordCountVisitor;
import wordCount.visitors.TreeVisitorI;
import wordCount.visitors.GrepVisitor;
import wordCount.visitors.PopulateTreeVisitor;

public class Driver {
	public static void main(String [] args) {
		//code executed here
		
		//String inputFile = args[0];
		//String outputFile = args[1];
		
		if(args.length != 4) {
			System.out.println("The format must be <inputfile> <outputfile> <NUM_ITERATIONS>");
			System.exit(1);
		}
		int NUM_ITERATIONS = Integer.parseInt(args[2]);
		if(args[0].endsWith(".txt") == false || args[1].endsWith(".txt") == false || NUM_ITERATIONS < 0) {
			System.out.println("The format must be <inputfile> <outputfile> <NUM_ITERATIONS>");
			System.exit(1);	
		}
		else if((args.length == 4) && (args[0].endsWith(".txt") == true) && (args[1].endsWith(".txt") == true)) {
			if(NUM_ITERATIONS > 0) {	
				long startTime = System.currentTimeMillis();
				System.out.println(startTime);
				for(int i = 0; i < NUM_ITERATIONS; i++) {
					BST bst = new BST();
					FileProcessor fp = new FileProcessor(args[0]);
					TreeVisitorI ptv = new PopulateTreeVisitor(fp);
					TreeVisitorI wcv = new WordCountVisitor(args[1]);
					TreeVisitorI grv = new GrepVisitor(args[1], args[3]);
					bst.accept(ptv);
					bst.accept(wcv);
					bst.accept(grv);
				}
				long finishTime = System.currentTimeMillis();
				System.out.println(finishTime);
				long total_time = (finishTime - startTime) / NUM_ITERATIONS;
				System.out.println(total_time);
			}
		}
}
}
